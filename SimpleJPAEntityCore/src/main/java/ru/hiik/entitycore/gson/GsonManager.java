/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.entitycore.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 *
 * @author vaganovdv
 */
public class GsonManager
{

 
    private Gson gson;
    public GsonManager()
    {
    
    }
    
    public void init()
    {
       GsonBuilder builder = new GsonBuilder();
       gson = builder.create();
        
    }        
    
    
    /**
     * @return the gson
     */
    public Gson getGson()
    {
        return gson;
    }
    
    
}
